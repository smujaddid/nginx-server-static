FROM nginx:1.21-alpine

ARG BUILD_DATE
ARG VCS_REF

LABEL maintainer="Sibghatullah Mujaddid" \
      org.label-schema.name="smujaddid/nginx-server-static" \
      org.label-schema.description="Static server using nginx and sftp" \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.schema-version="1.0" \
      org.label-schema.vcs-url="https://gitlab.com/smujaddid/nginx-server-static" \
      org.label-schema.vcs-ref=$VCS_REF

RUN echo "@community http://dl-cdn.alpinelinux.org/alpine/v3.12/community" >> /etc/apk/repositories \
    && apk -U add --no-cache \
        bash \
        git \
        jq \
        openssh \
        openssh-sftp-server \
        shadow@community \
        su-exec \
        supervisor \
        wget \
    && su-exec nobody true \
    && mkdir -p /var/log/supervisor \
    && mkdir -p /var/run/sshd \
    && rm -f /etc/ssh/ssh_host_*key* \
    && rm -fr /var/cache/apk/*

ENV SFTP_HOME /home/sftp
ENV SERVER_ROOT /home/server
ENV STORAGE_ROOT $SERVER_ROOT/storage
ENV PUBLIC_PATH public
ENV NGINX_AUTO_INDEX off
ENV NGINX_PORT 80
ENV NGINX_REAL_IP_FROM 0.0.0.0/0

RUN useradd -m -s /sbin/nologin -U sftp \
    && mkdir -p $SERVER_ROOT \
    && mkdir -p $STORAGE_ROOT \
    && chown -f root.root /home \
    && chown -Rf root.root $SERVER_ROOT \
    && chown -Rf sftp.sftp $STORAGE_ROOT \
    && chmod -Rf 700 $SFTP_HOME \
    && echo "alias sunx=\"su-exec nginx\"" >> /root/.bashrc \
    && echo "alias sunx=\"su-exec nginx\"" >> /root/.profile \
    && echo "alias suns=\"su-exec sftp\"" >> /root/.bashrc \
    && echo "alias suns=\"su-exec sftp\"" >> /root/.profile

COPY conf/ /usr/src/conf/
COPY conf/sshd_config /etc/ssh/sshd_config
COPY conf/supervisord.conf /etc/supervisord.conf

COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY conf/nginx.default.conf /etc/nginx/templates/default.conf.template

COPY scripts/sync-storage /usr/bin/sync-storage
COPY scripts/setup-storage.sh /
COPY scripts/start.sh /
COPY scripts/entrypoint.sh /
RUN chmod 755 /usr/bin/sync-storage \
    && chmod 755 /setup-storage.sh \
    && chmod 755 /start.sh \
    && chmod 755 /entrypoint.sh

EXPOSE 22

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/start.sh"]
