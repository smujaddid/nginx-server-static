[![pipeline status](https://gitlab.com/smujaddid/nginx-server-static/badges/master/pipeline.svg)](https://gitlab.com/smujaddid/nginx-server-static/-/commits/master)
[![docker hub](https://img.shields.io/docker/pulls/smujaddid/nginx-server-static.svg?style=flat)](https://hub.docker.com/r/smujaddid/nginx-server-static)
[![docker hub](https://img.shields.io/docker/stars/smujaddid/nginx-server-static.svg?style=flat)](https://hub.docker.com/r/smujaddid/nginx-server-static)

# Overview

Serve your static files using nginx. The static files can be either pulled from git repository
or mounted via docker volume to /home/server/storage.  

If configured via git repository, this will automatically sync it to the repository via a cron
job for every 5 minutes. If using git repository, the files will be clone into `/home/server/storage`.

Nginx will serve its contents using root folder `/home/server/storage/public`. So, make sure your
repository contains a `public` folder inside. This is to make sure the `.git` folder won't be served
by nginx.

You can also upload files via sftp by configuring an sftp password using environment variable
SFTP_PASSWORD.

## Versioning

| Docker Tag | Git Release | Nginx Version | Alpine Version | Base Image |
|-----|-------|-----|--------|-----------|
| latest/1.0.0 | Master Branch |1.19.3 | 3.12 | nginx:1.19-alpine |

## Links
- [https://gitlab.com/smujaddid/nginx-server-static](https://gitlab.com/smujaddid/nginx-server-static)
- [https://hub.docker.com/repository/docker/smujaddid/nginx-server-static](https://hub.docker.com/repository/docker/smujaddid/nginx-server-static)

# Usage

## Pull

```shell
$ docker pull smujaddid/nginx-server-static
```

## Run

### Using docker run

```shell
$ docker run -it --rm \
    -e 'SFTP_PASSWORD=test' \
    -e 'STORAGE_GIT_REPO=git@gitlab.com:username/repo-name.git' \
    -e 'STORAGE_GIT_NAME=Kimi no Nawa' \
    -e 'STORAGE_GIT_EMAIL=me@example.com' \
    -e 'STORAGE_GIT_USERNAME=username' \
    -e 'STORAGE_GIT_PERSONAL_TOKEN=access_token' \
    -e 'STORAGE_GIT_BRANCH=master' \
    -v /path/to/storage:/home/server/storage \
    -p 2222:22
    -p 8000:80
    -d --name static-server \
    smujaddid/nginx-server-static
```

### Using docker compose

```yaml
version: "3.7"

services:
  static-server:
    image: smujaddid/nginx-server-static:latest
    container_name: static-server
    environment:
      - SFTP_PASSWORD=test
      - STORAGE_GIT_REPO=git@gitlab.com:username/repo-name.git
      - STORAGE_GIT_NAME=Kimi no Nawa
      - STORAGE_GIT_EMAIL=me@example.com
      - STORAGE_GIT_USERNAME=username
      - STORAGE_GIT_PERSONAL_TOKEN=access_token
      - STORAGE_GIT_BRANCH=master
    ports:
      - "2222:22"
      - "8000:80"
```

## Laravel

You can connect this to your laravel application via sftp configured file storage.
You can read the [documentation here](https://laravel.com/docs/7.x/filesystem).

### Configuration

The following is an example of a storage disk configuration in `config/filesystem.php`:

```php
    'disks' => [
        'uploads' => [
            'driver' => 'sftp',
            'host' => env('STATIC_SERVER_HOST', 'static-server'),
            'port' => env('STATIC_SERVER_PORT', 22),
            'username' => env('STATIC_SERVER_USERNAME', 'sftp'),
            'password' => env('STATIC_SERVER_PASSWORD', 'test'),
            'root' => env('STATIC_SERVER_ROOT', 'storage/public'),
            'timeout' => env('STATIC_SERVER_TIMEOUT', 10),
            'directory_perm' => 0755,
            'url_prefix' => env('STATIC_SERVER_URL_PREFIX', 'http://localhost:8000'),
        ],
    ],
```

Note: Make sure your `storage` folder have the `public` folder inside

### Usage

If your default `disk` is not `uploads` then make sure to call the `disk` method first.

```php
Storage::disk('uploads')->put('avatars/1', $fileContents);
```

# License

MIT

Copyright (c) 2020 Sibghatullah Mujaddid
