#!/bin/sh
set -e

# Generate unique ssh keys for this container, if needed
if [ ! -f /etc/ssh/ssh_host_ed25519_key ]; then
  ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ''
fi
if [ ! -f /etc/ssh/ssh_host_rsa_key ]; then
  ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ''
fi

# Restrict access from other users
chmod 600 /etc/ssh/ssh_host_ed25519_key || true
chmod 600 /etc/ssh/ssh_host_rsa_key || true

# Setup custom user ID and group ID for sftp user

if [ ! -z "${SFTP_PASSWORD}" ]; then
  echo "sftp:${SFTP_PASSWORD}" | chpasswd
fi

if [ -z "${NGINX_ENTRYPOINT_QUIET_LOGS:-}" ]; then
  exec 3>&1
else
  exec 3>/dev/null
fi

if [ "$1" == "/start.sh" ]; then
  if /usr/bin/find "/docker-entrypoint.d/" -mindepth 1 -maxdepth 1 -type f -print -quit 2>/dev/null | read v; then
    echo >&3 "$0: /docker-entrypoint.d/ is not empty, will attempt to perform configuration"

    echo >&3 "$0: Looking for shell scripts in /docker-entrypoint.d/"
    find "/docker-entrypoint.d/" -follow -type f -print | sort -n | while read -r f; do
      case "$f" in
        *.sh)
          if [ -x "$f" ]; then
            echo >&3 "$0: Launching $f";
            "$f"
          else
            # warn on shell scripts without exec bit
            echo >&3 "$0: Ignoring $f, not executable";
          fi
          ;;
        *) echo >&3 "$0: Ignoring $f";;
      esac
    done

    echo >&3 "$0: Configuration complete; ready for start up"
  else
    echo >&3 "$0: No files found in /docker-entrypoint.d/, skipping configuration"
  fi
fi

if [ "${1#-}" != "${1}" ] || [ -z "$(command -v "${1}")" ]; then
  set -- supervisorctl "$@"
fi

exec "$@"
