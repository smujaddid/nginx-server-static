#!/bin/bash

GIT_CONFIG_CMD="git config --global"

# Setup git variables
if [ ! -z "$STORAGE_GIT_EMAIL" ]; then
  $GIT_CONFIG_CMD user.email "$STORAGE_GIT_EMAIL"
fi
if [ ! -z "$STORAGE_GIT_NAME" ]; then
  $GIT_CONFIG_CMD user.name "$STORAGE_GIT_NAME"
  $GIT_CONFIG_CMD push.default simple
fi

if [ ! -z "$STORAGE_GIT_USERNAME" ] && [ ! -z "$STORAGE_GIT_PERSONAL_TOKEN" ]; then
  # Setup Gitlab Url with access token for root user
  $GIT_CONFIG_CMD --add url."https://${STORAGE_GIT_USERNAME}:${STORAGE_GIT_PERSONAL_TOKEN}@gitlab.com/".insteadOf "https://gitlab.com/"
  $GIT_CONFIG_CMD --add url."https://${STORAGE_GIT_USERNAME}:${STORAGE_GIT_PERSONAL_TOKEN}@gitlab.com/".insteadOf "git@gitlab.com:"
fi

# Dont pull code down if the .git folder exists
if [ ! -d "$STORAGE_ROOT/.git" ]; then
  # Pull down code from git for our site!
  if [ ! -z "$STORAGE_GIT_REPO" ]; then
    # Remove the test index file if you are pulling in a git repo
    if [ ! -z ${REMOVE_FILES} ] && [ ${REMOVE_FILES} == 0 ]; then
      echo "skiping removal of files"
    else
      rm -Rf $STORAGE_ROOT/*
    fi
    GIT_COMMAND='git clone --depth=1 -q '
    if [ ! -z "$STORAGE_GIT_BRANCH" ]; then
      GIT_COMMAND=${GIT_COMMAND}" -b ${STORAGE_GIT_BRANCH}"
    fi
    
    GIT_COMMAND=${GIT_COMMAND}" ${STORAGE_GIT_REPO}"

    ${GIT_COMMAND} $STORAGE_ROOT || exit 1
    if [ ! -z "$STORAGE_GIT_TAG" ]; then
      git checkout ${GIT_TAG} || exit 1
    fi
    if [ ! -z "$STORAGE_GIT_COMMIT" ]; then
      git checkout ${GIT_COMMIT} || exit 1
    fi
  fi
fi

if [ -z "$SKIP_FIRST_SYNC" ]; then
  # Immediately trying to sync storage
  if [ -f "/usr/bin/sync-storage" ]; then
    /usr/bin/sync-storage >> /home/sftp/sync-storage.log
  fi
fi

# Run custom scripts
if [[ "$RUN_SCRIPTS" == "1" ]] ; then
  if [ -d "$STORAGE_ROOT/scripts/" ]; then
    # make scripts executable incase they aren't
    chmod -Rf 750 $STORAGE_ROOT/scripts/*; sync;
    # run scripts in number order
    for i in `ls $STORAGE_ROOT/scripts/`; do $STORAGE_ROOT/scripts/$i ; done
  else
    echo "Can't find script directory"
  fi
fi
