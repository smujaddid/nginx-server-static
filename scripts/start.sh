#!/bin/bash

chown -Rf sftp.sftp $STORAGE_ROOT
su-exec sftp '/setup-storage.sh'

if [ -z "$PUBLIC_PATH" ]; then
  PUBLIC_PATH=public
fi

# Make sure the public folder exists
if [ ! -d "$STORAGE_ROOT/$PUBLIC_PATH" ]; then
  mkdir -p $STORAGE_ROOT/$PUBLIC_PATH
  chown -f sftp.sftp $STORAGE_ROOT/$PUBLIC_PATH
fi

# Setup cron job
if [ -z "$SKIP_AUTO_SYNC" ]; then
  cp /usr/src/conf/crontab /etc/crontabs/sftp
  if [ ! -z "$CRON_SCHEDULE" ]; then
    sed -i "s#[*]/5 [*] [*] [*] [*] /usr/bin/sync-storage#${CRON_SCHEDULE} /usr/bin/sync-storage#g" /etc/crontabs/sftp
  fi
  chown -Rf root.root /etc/crontabs/sftp
fi

# Start supervisord and services
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
